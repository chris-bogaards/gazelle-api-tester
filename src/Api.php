<?php

namespace Gazelle;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class Api
{

    /**
     * @var array
     */
    public $responsetimes = [];

    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var null|string
     */
    protected $bearer;

    /**
     * @var bool
     */
    protected $output;

    /**
     * @param array $config
     * @param bool  $output Echo requests output to stdout
     */
    public function __construct(array $config, bool $output = true)
    {
        $this->csvPath = $config['csv_path'];
        $this->client = new Client(array_merge([
            RequestOptions::HTTP_ERRORS => false
        ], $config));

        if (!file_exists($this->csvPath)) {
            $handle = fopen($this->csvPath, "a");
            fputcsv($handle, ['date', 'duration', 'method', 'url', 'status','reason']);
            fclose($handle);
        }

        $this->output = $output;
    }

    /**
     * @param string $uri
     * @param array  $options
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $uri, array $options = [])
    {
        $response = $this->request("GET", $uri, array_merge([
            RequestOptions::HEADERS => [
                "Authorization" => $this->bearer,
                "User-Agent" => getenv("USERAGENT")
            ],
            $options
        ]));

        return $this->parseResponse($response);
    }

    /**
     * @param string $uri
     * @param array  $data
     * @param array  $options
     *
     * @return mixed|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post(string $uri, array $data, array $options = [])
    {
        $response = $this->request("POST", $uri, array_merge([
            RequestOptions::JSON => $data,
            RequestOptions::HEADERS => [
                "Authorization" => $this->bearer,
                "User-Agent" => getenv("USERAGENT")
            ]
        ], $options));

        return $this->parseResponse($response);
    }

    /**
     * @param string $uri
     * @param array  $data
     * @param array  $options
     *
     * @return mixed|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(string $uri, array $data = [], array $options = [])
    {
        $response = $this->request("DELETE", $uri, array_merge([
            RequestOptions::JSON => $data,
            RequestOptions::HEADERS => [
                "Authorization" => $this->bearer,
                "User-Agent" => getenv("USERAGENT")
            ],
            $options
        ]));

        return $this->parseResponse($response);
    }

    /**
     * @param string $bearer
     */
    public function setBearer(string $bearer)
    {
        $this->bearer = $bearer;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $options
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function request(string $method, string $uri, array $options)
    {
        $start = microtime(true);

        $response = $this->client->request($method, $uri, $options);

        $end = microtime(true);

        $handle = fopen($this->csvPath, "a");
        fputcsv($handle, [
            'date' => (new \DateTime("now"))->format('Y-m-d H:i:s'),
            'duration' => number_format($end - $start, 4),
            'method' => strtoupper($method),
            'url' => $uri,
            'status' => $response->getStatusCode(),
            'reason' => $response->getReasonPhrase(),
        ]);
        fclose($handle);

        $total = str_pad(number_format($end - $start, 4), 8);
        $method = str_pad($method, 6);
        $uri = str_pad($uri, 16);

        $this->responsetimes[] = $end - $start;

        if ($this->output) {
            echo "${total} [ {$method} {$uri} ] --> {$response->getStatusCode()} {$response->getReasonPhrase()}\n";
        }

        return $response;
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface $response
     *
     * @return mixed|null
     */
    protected function parseResponse(ResponseInterface $response)
    {
        $status = $response->getStatusCode();
        $body = json_decode((string) $response->getBody()->getContents(), true);

        if ($status !== 200) {
            return null;
        }

        return $body;
    }
}


