<?php

use Gazelle\Api;

require 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$api = new Api([
    'base_uri' => getenv("BASE_URL"),
    'csv_path' => getenv("CSV_PATH")
], true);

foreach (range(1, getenv("SCENARIO_ITERATIONS")) as $batch) {
    runScenario($api);
}

/**
 * @param \Gazelle\Api $api
 *
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function runScenario($api)
{
    $response = $api->post("oauth2/token", [
        "grant_type" => "password",
        "client_id" => getenv("CLIENT_ID"),
        "client_secret" => getenv("CLIENT_SECRET"),
        "username" => getenv("USERNAME"),
        "password" => getenv("PASSWORD")
    ]);

    if ($response === null || !isset($response['access_token'])) {
        echo "Error fetching token\n";
        return;
    }

    $api->setBearer("Bearer {$response['access_token']}");

    $api->get("customers/me");
    $api->post("customers/frames", [
        'code' => getenv("REGISTRATION_CODE"),
        'frame' => getenv("FRAMENUMBER")
    ]);

    $api->get("customers/me");
    $api->get("customers/frames");

    $api->get("customers/me");
    $api->delete("customers/frames", [
        'code' => getenv("REGISTRATION_CODE"),
        'frame' => getenv("FRAMENUMBER")
    ]);

    $api->get("customers/me");
    $api->get("customers/frames");
}

$sum = array_sum($api->responsetimes);
$count = count($api->responsetimes);
$max = number_format(max($api->responsetimes), 3);
$min = number_format(min($api->responsetimes), 3);
$average = number_format($sum / $count, 3);

echo "Average from {$count} requests: {$average}\n";
echo "Max: {$max}\n";
echo "Min: {$min}\n\n";
